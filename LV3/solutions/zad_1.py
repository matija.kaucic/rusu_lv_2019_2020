import pandas as pd

def lbsToKg(lbs):
    kg = lbs*1000 / 2.2046
    return kg

mtcars = pd.read_csv(r'C:\Users\matij\rusu_lv_2019_2020\LV3\resources/mtcars.csv')


potrosnja = mtcars.sort_values(by=['mpg'])
print('Najveću potrošnju imaju sljedećih pet automobila: ', potrosnja.tail(5)[['car','mpg']])

osamCilindara = mtcars[mtcars.cyl==8].sort_values(by=['mpg'])
print('Najmanju potrošnju od automobila s osam cilindra imaju sljedeća tri automobila: ', osamCilindara.head(3)[['car','mpg']])

print('Srednja potrošnja automobila sa šest cilindara je: ', mtcars[mtcars.cyl==6].mpg.mean())

cetiriCilindra = mtcars[(mtcars.cyl == 4) & (mtcars.wt >= 2.0) & (mtcars.wt <= 2.2)]
print('Srednja potrošnja automobila sa četiri cilindra i mase između 2000 i 2200 lbs je: ', cetiriCilindra.mpg.mean())

rucna = len(mtcars[mtcars.am==1])
automatski = len(mtcars[mtcars.am==0])
print('Automobila s ručnim je: ', rucna,  ', a s automatskim mjenjačem: ', automatski)

print('Automobila s automatskim mjenjačem i snagom preko 100 konjskih snaga ima: ', len(mtcars[(mtcars.am==1) & (mtcars.hp>100)]))

masa = []
masa.append(lbsToKg(mtcars.wt))
print('Masa svakog automobila u kilogramima je: ', masa)