VJEŽBA 3:    RAD  S  PANDAS  PYTHON  BIBLIOTEKOM.  EKSPLORATIVNA  ANALIZA PODATAKA.
-------------------------------------------------------------------------------------
Cilj vježbe: Upoznati  se  s  načinom  korištenja  Pandas  biblioteke  za  programski  jezik  Python. Upoznati  se  s  eksplorativnom  analizom  podataka  pomoću  grafičkog  prikaza podataka.
-------------------------------------------------------------------------------------
Opis vježbe: U  vježbi  se  studenti  upoznaju  s  Pandas  bibliotekom za  programski  jezik  Python.  Ova  biblioteka  omogućava  relativno laganu  manipulaciju  podacima  te,  zajedno  s  grafičkom  bibliotekom,  omogućuje  dobivanje  uvida  u  karakteristike raspoloživih podataka (distribucije, srednje vrijednosti i sl.). Ovo je obično i prvi korak u problemima strojnog učenja, a poznat je i pod nazivom eksplorativna analiza podataka. 
-------------------------------------------------------------------------------------

Zadatak 1:

CSV dokument smo učitali pomoću naredbe pandas.read_csv. Podacima iz csv datoteke smo pristupali preko operatora točke i navođenjem naziva željenog atributa. Za prebacivanje iz lbs u kilograme smo definirali vlastitu funkciju.


Zadatak 2:

Bar plot smo koristili grupiranjem istih tipova podataka poput cilindara i zatim prikazali informacije za svaki od tih podskupova. Konkretno, u prvom dijelu smo koristili groupby() metodu i zatim plotali po mpg.mean(), odnosno srednjoj vrijednosti potrošnje.
Box plot smo koristili tako da bi istakli stupac koji bi htjeli promatrati naredbom column='' te by = [''] u kojoj odredimo po kojim podatcima da grupiramo rezultate.
Usporedbu potrošnje automobila s ručnim i automatskim mjenjačem smo vršili pomoću box plota. Po rezultatima smo zaključili da automobili s ručnim mjenjačem imaju veću potrošnju od onih s automatskim.
Odnos ubrzanja i snage automobila s ručnim odnosno automatskim mjenjačem smo prikazali scatter plotom.


Zadatak 3:

Za prvi dio zadatka samo smo uredili skriptu iz mape resources. Izmijenili smo par stvari: pod url smo stavili generirani url s web stranice o kvaliteti zraka u Republici Hrvatskoj. Guglanjem smo riješili dva errora: airQualityHR = urllib.urlopen(url).read() nije bila ispravna linija, novije verzije pythona zahtjevaju ključnu riječ request prije naredbe urlopen(url). Također, linija df.vrijeme = pd.to_datetime(df.vrijeme) nije bila ispravna, morali smo u zagradu dodati još jedan atribut a to je utc=True.

Drugi dio zadatka smo napravili po uzoru na zadatak 1.

Treći dio smo realizirali uporabom barplota i uspoređivanjem očekivanog broja dana po mjesecu s brojem mjerenja tog mjeseca, broj koji je predstavljao razliku smo prikazali na grafu.

U četvrtom dijelu smo pomoću boxplota usporedili rezultate mjerenja za prosinac i kolovoz. Definirali smo varijablu mjeseci koja sadrži df mjeseca koji je redom ili 12. ili 8. pa smo poslije plotali tu varijablu po mjerenjima u odnosu na mjesece.
U petom smo definirali vikend kao true ako je broj dana preko 4 (zato što ponedjeljak ima vrijednost za dayOfweek 0, pa subota i nedjelja imaju 6 i 7). Zatim smo boxplotali mjerenja po varijabli vikend i dobili rezultate mjerenja odnosa koncentracije PM10 između vikenda i radnih dana, iskreno ne znam što zaključiti iz rezultata, ali najveća koncentracija je bila za vikend.