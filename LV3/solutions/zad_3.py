import urllib
import pandas as pd
import xml.etree.ElementTree as ET
import matplotlib.pyplot as plt

# url that contains valid xml file:
url = 'http://iszz.azo.hr/iskzl/rs/podatak/export/xml?postaja=160&polutant=5&tipPodatka=5&vrijemeOd=01.01.2017&vrijemeDo=31.12.2017' # 1.dio - Dohvaćanje mjerenja dnevne koncentracije lebdećih čestica PM10 za 2017. godinu za grad Osijek.

airQualityHR = urllib.request.urlopen(url).read()
root = ET.fromstring(airQualityHR)

df = pd.DataFrame(columns=('mjerenje', 'vrijeme'))

i = 0
while True:
    
    try:
        obj = root.getchildren()[i].getchildren()
    except:
        break
    
    row = dict(zip(['mjerenje', 'vrijeme'], [obj[0].text, obj[2].text]))
    row_s = pd.Series(row)
    row_s.name = i
    df = df.append(row_s)
    df.mjerenje[i] = float(df.mjerenje[i])
    i = i + 1

df.vrijeme = pd.to_datetime(df.vrijeme, utc=True)
df.plot(y='mjerenje', x='vrijeme');

# add date month and day designator
df['month'] = df['vrijeme'].dt.month
df['dayOfweek'] = df['vrijeme'].dt.dayofweek


#2. dio - Ispis tri datuma u godini kada je koncentracija PM10 bila najveća.
najvecaKonc = df.sort_values(['mjerenje'])
print('Tri danas najvećom koncentracijom čestica: ', najvecaKonc.tail(3)[['dayOfweek','month','vrijeme','mjerenje']])

#3.dio - Pomoću barplot prikažite ukupni broj izostalih vrijednosti tijekom svakog mjeseca.
plt.figure(2)
brojDana = [31,28,31,30,31,30,31,31,30,31,30,31] - df.groupby('month').mjerenje.count()
brojDana.plot.bar()
plt.xlabel('Redni broj mjeseca')
plt.ylabel('Broj izostalih mjerenja')
plt.title('Broj izostalih mjerenja tijekom godine')
plt.grid(True)

#4.dio - Pomoću boxplot usporedite PM10 koncentraciju tijekom jednog zimskog i jednog ljetnog mjeseca.
mjeseci = df[(df.month == 12) | (df.month == 8)]
mjeseci.boxplot(column = ['mjerenje'], by = 'month')
plt.title('Odnos koncentracija PM10 između kolovoza i prosinca')
plt.suptitle('')
plt.xlabel('Redni broj mjeseca')
plt.ylabel('Koncentracija PM10')

#5.dio - Usporedbu distribucije PM10 čestica tijekom radnih dana s distribucijom čestica tijekom vikenda.
plt.figure(4)
df['vikend'] = (df.dayOfweek > 4) #zato što je ponedjeljak označen s nulom, dakle subota i nedjelja pod dayOfweek imaju vrijednosti 5 i 6
df.boxplot(column = ['mjerenje'], by = 'vikend')
plt.title("Odnos koncentracije PM10 između radnih dana i vikenda")
plt.suptitle("")
plt.xlabel('Dan')
plt.ylabel('Koncentracija PM10')
plt.xticks([1, 2], ['Radni dan', 'Vikend'])