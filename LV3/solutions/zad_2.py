import pandas as pd
import matplotlib.pyplot as plt

mtcars = pd.read_csv('../resources/mtcars.csv')

# #Bar plot
# barPlot = mtcars.groupby(mtcars.cyl).mpg.mean()
# barPlot.plot.bar()
# plt.xlabel('Cilindri')
# plt.ylabel('Prosječna potrošnja u mpg')
# plt.title('Ovisnost broja cilindara i potrošnje')
# plt.grid(True)

# #Box plot
# mtcars.boxplot(column = 'wt', by = ['cyl'])
# plt.xlabel('Cilindri')
# plt.ylabel('Težina u lbs/1000')
# plt.suptitle('Težina automobila s različitim brojem cilindara')
# plt.title('')
# plt.grid(True)

# #Usporedba potrošnje automobila s ručnim i automatskim mjenjačem - s ručnim mjenjačem imaju veću potrošnju
# mtcars.boxplot(column = 'mpg', by = ['am'])
# plt.xlabel('Mjenjač')
# plt.ylabel('Potrošnja u mpg')
# plt.suptitle('Potrošnja automobila s ručnim i automatskim mjenjačem')
# plt.title('')
# plt.xticks([1, 2], ['automatski', 'ručni'])
# plt.grid(True)

#Odnos ubrzanja i snage automobila za automobile s ručnim odnosno automatskim mjenjačem
plt.scatter(mtcars[mtcars.am == 0].qsec, mtcars[mtcars.am == 0].hp, label='automatski')
plt.scatter(mtcars[mtcars.am == 1].qsec, mtcars[mtcars.am == 1].hp, label='ručni')
plt.xlabel('Ubrzanje')
plt.ylabel('Broj konjskih snaga')
plt.title('Odnos ubrzanja i snage za automobile s ručnim odnosno automatskim mjenjačem')
plt.legend()
plt.grid(True)

