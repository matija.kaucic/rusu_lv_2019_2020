from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans

def generate_data(n_samples, flagc):
    
    if flagc == 1:
        random_state = 365
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        
    elif flagc == 2:
        random_state = 148
        X,y = datasets.make_blobs(n_samples=n_samples, random_state=random_state)
        transformation = [[0.60834549, -0.63667341], [-0.40887718, 0.85253229]]
        X = np.dot(X, transformation)
        
    elif flagc == 3:
        random_state = 148
        X, y = datasets.make_blobs(n_samples=n_samples,
                          cluster_std=[1.0, 2.5, 0.5, 3.0],
                          random_state=random_state)

    elif flagc == 4:
        X, y = datasets.make_circles(n_samples=n_samples, factor=.5, noise=.05)
        
    elif flagc == 5:
        X, y = datasets.make_moons(n_samples=n_samples, noise=.05)
    
    else:
        X = []
        
    return X

data = generate_data(500, 1)
plt.figure(2)
# plt.scatter(data[:,0], data[:,1])

# kmeans = KMeans(n_clusters=3,max_iter=500,)
# pred_y = kmeans.fit_predict(data)

criterion_func = []
cluster_number = []

for i in range(1, 20):
    kmeans = KMeans(n_clusters=i)
    pred_y = kmeans.fit_predict(data)
    criterion_score = kmeans.score(data)
    criterion_func.append(criterion_score)
    cluster_number.append(i)

plt.scatter(cluster_number, criterion_func)