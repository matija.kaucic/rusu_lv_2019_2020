import scipy as sp
from sklearn import cluster, datasets
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg 

imageNew = mpimg.imread('../resources/example_grayscale.png')
    
X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=2,n_init=1) #što je manji broj klastera slika je tamnija (ako su 2 klastera, postoji samo crno i bijelo na slici), što je veći broj ima više nijansi i slika je svjetlija i bolje kvalitete
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageNew_compressed = np.choose(labels, values)
imageNew_compressed.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew,  cmap='gray')

plt.figure(2)
plt.imshow(imageNew_compressed,  cmap='gray')