import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg 
from sklearn.cluster import KMeans
from sklearn import datasets
from sklearn import cluster, datasets

imageNew = mpimg.imread('../resources/example.png')

n_colors = 2 #tu mijenjamo vrijednosti, minimalno je 2 i tada je slika dosta loše kvalitete, na n=20 se praktički ni ne vidi razlika s originalom

X = imageNew.reshape((-1, 1)) # We need an (n_sample, n_feature) array
k_means = cluster.KMeans(n_clusters=n_colors,n_init=1) 
k_means.fit(X) 
values = k_means.cluster_centers_.squeeze()
labels = k_means.labels_
imageNew_quant = np.choose(labels, values)
imageNew_quant.shape = imageNew.shape

plt.figure(1)
plt.imshow(imageNew)
plt.title("Originalna slika")

plt.figure(2)
plt.imshow(imageNew_quant)
plt.title("Kvantizirana slika")