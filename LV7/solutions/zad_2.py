import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from sklearn import neural_network as nn


def add_noise(y):
	
	np.random.seed(14)
	varNoise = np.max(y) - np.min(y)
	y_noisy = y + 0.1*varNoise*np.random.normal(0,1,len(y))
	return y_noisy


def non_func(n):
	
	x = np.linspace(1,10,n)
	y = 1.6345 - 0.6235*np.cos(0.6067*x) - 1.3501*np.sin(0.6067*x) - 1.1622 * np.cos(2*x*0.6067) - 0.9443*np.sin(2*x*0.6067)
	y_measured = add_noise(y)
	data = np.concatenate((x,y,y_measured),axis = 0)
	data = data.reshape(3,n)
	return data.T

trainingData = non_func(500)
testData = non_func(500)
x_train = trainingData[:, 0].reshape(-1, 1)
y_train = trainingData[:, 1]
y_train_real = trainingData[:, 2]
x_test = testData[:, 0].reshape(-1, 1)
y_test = testData[:, 1].reshape(-1, 1)


network = nn.MLPRegressor((500, 50), max_iter=10000)
network.fit(x_train, y_train)
predict = network.predict(x_test)
network.fit(x_train, y_train_real)
predict_real = network.predict(x_test)

print('MSE iznosi: ', mean_squared_error(y_test, predict))

plt.scatter(x_train, y_train_real, c='g')
plt.plot(x_test, y_test, label='Test', c='r')
plt.plot(x_test, predict, label='Linear regression', c='y')
plt.plot(x_test, predict_real, label='Real', c='b')
plt.legend()
plt.show()