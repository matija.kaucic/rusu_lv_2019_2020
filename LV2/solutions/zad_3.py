# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 22:12:37 2020

@author: matij
"""

import numpy as np
import matplotlib.pyplot as plt


def prosjek_muski():
    prosjek = sum(visina[spol==1])/len(visina[spol==1])
    return prosjek

def prosjek_zenski():
    prosjek = sum(visina[spol==0]/len(visina[spol==0]))
    return prosjek


spol = np.random.randint(2, size = 1000)
visina = np.ones(1000, dtype = int)

visina[spol==1] = np.random.normal(180, 7, len(visina[spol==1]))
visina[spol==0] = np.random.normal(167, 7, len(visina[spol==0]))

plt.hist(visina[spol==1], bins=50, color='b')
plt.hist(visina[spol==0], bins=50, color='r')

plt.axvline(prosjek_muski(), color="darkblue")
plt.axvline(prosjek_zenski(), color="darkred")

plt.title('Visine muškaraca i žena')
plt.xlabel('Visina u centimetrima')
plt.ylabel('Broj ljudi')
plt.legend(['Prosjek - muškarci','Prosjek - žene','Muškarci','Žene'])