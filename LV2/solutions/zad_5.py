# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 23:14:46 2020

@author: matij
"""

import matplotlib.pyplot as plt
import pandas as pd

auti = pd.read_csv ('mtcars.csv')

potrosnja = auti.mpg
konjske_snage = auti.hp
tezine = auti.wt

plt.scatter(potrosnja, konjske_snage, c=auti.wt)
plt.colorbar().set_label('Težina (lbs / 1000)')
plt.title('Ovisnost  potrošnje  automobila  (mpg)  o konjskim snagama  (hp)')
plt.xlabel('Potrošnja automobila (mpg)')
plt.ylabel('Konjske snage (hp)')

print('Minimalna potrošnja automobila: ', min(auti.mpg))
print('Maksimalna potrošnja automobila: ', max(auti.mpg))
print('Srednja vrijednost potrošnje automobila: ', sum(auti.mpg)/len(auti.mpg))