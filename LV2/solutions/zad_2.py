# -*- coding: utf-8 -*-
"""
Created on Sat Nov 28 08:59:09 2020

@author: student
"""

import re

email_lista = []
prviZadatak = []
drugiZadatak = []
treciZadatak = []
cetvrtiZadatak = []
petiZadatak = []

fhand = open('mbox-short.txt')

for line in fhand:
    emailovi = re.findall('[a-zA-Z0-9._]+@[a-zA-Z0-9._]+', line) 
    for email in emailovi:
        email_lista.append(email.split('@')[0])
        
        
for email in email_lista:
    emailovi = re.findall('\S*a\S*', email) #1. dio - sadrži najmanje jedno slovo a 
    for line in emailovi:
        prviZadatak.append(line)  


for email in email_lista:
    emailovi = re.findall('^[^a]*a[^a]*$', email) #2. dio - sadrži točno jedno slovo a
    for line in emailovi:
        drugiZadatak.append(line)


for email in email_lista:
    emailovi = re.findall('^[^a]+$', email) #3. dio - ne sadrži slovo a
    for line in emailovi:
        treciZadatak.append(line)
        

for email in email_lista:
    emailovi = re.findall('\S*[0-9]\S*', email) #4. dio - sadrži jedan ili više numeričkih znakova (0 – 9)
    for line in emailovi:
        cetvrtiZadatak.append(line)


for email in email_lista:
    emailovi = re.findall('[a-z]+', email) #5. dio - sadrži samo mala slova (a-z)
    for line in emailovi:
        petiZadatak.append(line)
        

# print(prviZadatak)
# print(drugiZadatak)
# print(treciZadatak)
# print(cetvrtiZadatak)
print(petiZadatak)