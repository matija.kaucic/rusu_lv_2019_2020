# -*- coding: utf-8 -*-
"""
Created on Tue Dec  1 23:01:57 2020

@author: matij
"""

import numpy as np
import matplotlib.pyplot as plt

kocka = np.random.randint(low=1, high=7, size=100)

plt.hist(kocka, bins=50)

plt.title('Rezultati bacanja kockice')
plt.xlabel('Broj na kockici')
plt.ylabel('Broj pojavljivanja')