# -*- coding: utf-8 -*-
"""
Created on Wed Dec  2 01:07:13 2020

@author: matij
"""

import matplotlib.pyplot as plt 
import matplotlib.image as mpimg 


img = mpimg.imread('tiger.png') 
imgplot = plt.imshow(img)

img = img * 2             
imgplot = plt.imshow(img)