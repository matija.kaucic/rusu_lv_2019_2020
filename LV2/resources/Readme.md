### Opis dataseta mtcars
Motor Trend Car Road Tests

The data was extracted from the 1974 Motor Trend US magazine, and comprises fuel consumption and 10 aspects of automobile design and performance for 32 automobiles (1973â€“74 models
* mpg - Miles/(US) gallon
* cyl - Number of cylinders
* disp - Displacement (cu.in.)
* hp - Gross horsepower
* drat - Rear axle ratio
* wt - Weight (lb/1000)
* qsec - 1/4 mile time
* vs - V/S
* am - Transmission (0 = automatic, 1 = manual)
* gear - Number of forward gears
* carb - Number of carburetors

Source: R built in dataset

-------------------------------------------------------------------------------------------------------------------------------------------------------

**Izvještaj s vježbe:**


**Zadatak 1** 

Napišite  Python  skriptu  koja će  učitati  tekstualnu  datoteku,  pronaći  valjane  email  adrese  te  izdvojiti  samo  prvi  dio adrese (dio ispred znaka @). Koristite odgovarajući regularni izraz. Koristite datoteku mbox-short.txt. Ispišite rezultat.
-------------------------------------------------------------------------------------------------------------------------------------------------------

Otvaranje datoteke smo postigli pomoću naredbe open. Pronalazak valjanih e-mail adresa je bio moguć uporabom naredbe re.findall u kojoj smo upotrijebili odgovarajući regularni izraz. Valjane e-mail adrese smo spremali u listu i odmah im "odsjekli" znak @ i sve desno od znaka @. Na kraju smo pomoću anredbe print ispisali listu i samim time sve odgovarajuće email adrese (dio ispred znaka@).



**Zadatak 2**

Na temelju rješenja prethodnog zadatka izdvojite prvi dio adrese (dio ispred @ znaka) samo ako: 
	1.sadrži najmanje jedno slovo a 
	2.sadrži točno jedno slovo a 
	3.ne sadrži slovo a 
	4.sadrži jedan ili više numeričkih znakova (0 – 9) 
	5.sadrži samo mala slova (a-z)
-------------------------------------------------------------------------------------------------------------------------------------------------------

Otvaranje datoteke smo postigli kao i u prošlom zadatku. Zatim smo kopirali cijeli kod kao iz prošlog zadatka u kojem smo definirali valjanje email adrese i dodali smo ih u listu emailova. Za svaki od pet dijelova ovog zadatka smo napravili po jednu listu u koju ćemo spremiti odgovarajuća rješenja. Za svaki dio, prođemo kroz listu emailova i u njoj pretražujemo ženjene rezultate pomoću odgovarajućeg regularnog izraza te istim principom appendamo valjanje stringove u jednu od prije definiranih listi. 1. dio traži da nađemo mailove koji sadrže jedno slovo a, a to postignemo regularnim izrazom: \S*a\S* koji iznačava da ispred i iza može biti bilo koji znak osim praznog znaka. Za 2. dio mailove koji sadrže točno jedno slovo a, izraz: ^[^a]*a[^a]*$ gdje ^ označava početak niza, ^a da se a ne nalazi u traženom skupu, a da se traži znak a te $ koji označava kraj skupa. ^[^a]+$ označava slično kao u prošlom primjeru, samo što ovdje ne sadrži slovo a. \S*[0-9]\S* označava string koji sadržava jedan ili više numeričkih znakova. [a-z]+ označava string koji sadrži samo mala slova.   



**Zadatak 3**

Napravite  jedan  vektor  proizvoljne  dužine  koji  sadrži  slučajno  generirane  cjelobrojne  vrijednosti  0  ili  1.  Neka  1 označava mušku osobu, a 0 žensku osobu. Napravite drugi vektor koji sadrži visine osoba koje se dobiju uzorkovanjem odgovarajuće  distribucije.  U  slučaju  muških  osoba  neka  je  to  Gaussova  distribucija  sa  srednjom  vrijednošću  180  cm  i standardnom devijacijom 7 cm, dok je u slučaju ženskih osoba to Gaussova distribucija sa srednjom vrijednošću 167 cm i  standardnom  devijacijom  7  cm.  Prikažite  podatke  te  ih  obojite  plavom  (1)  odnosno  crvenom  bojom  (0). Napišite funkciju  koja  računa  srednju  vrijednost  visine  za  muškarce  odnosno  žene  (probajte  izbjeći  for  petlju  pomoću  funkcije np.dot). Prikažite i dobivene srednje vrijednosti na grafu.
-------------------------------------------------------------------------------------------------------------------------------------------------------

Vektor koji sadrži slučajno generirane cjelobrojne vrijednosti 0 ili 1 smo ostvarili naredbom random.randomint(2, size) koja je u sklopu numpy biblioteke. Drugu matricu smo prvo definirali i popunili jedinicama čisto da bi kasnije popunili tu matricu temeljem rezultata prve matrice. To radimo tako što ispitamo sve članove prve matrice imaju li vrijednost 1 ili 0. Ako imaju vrijednost 1, onda se radi o muškarcima i radimo Gaussovu razdiobu na temelju njihovih podataka, ako je 0, radi se o ženama i tada radimo odgovarajuću razdiobu. Za element veličine koji prima np.random.randomint metoda koristimo len(visina[spol==1]) ili len(visina[spol==0]) ovisno o kojem se spolu radi. Dobivene rezultate prikazujemo histogramom pomoću metode plt.hist(). Funkcije koje računaju srednju vrijednost smo napisali jednostavno, zbrojimo sve element prve matrice koji za vrijednost spola imaju 1 i podijelimo s njihovom dužinom, isto tako i za srednju vrijednost kod žena. Na kraju, te dvije srednje vrijednost pokazujemo na grafu pomoću naredbe plt.axvline koja povlači okomitu crtu na mjestu gdje je prosječna vrijednost. Uz sve to, dodali smo i naslov, labele na x i y osi te legendu za boje.



**Zadatak 4 **

Simulirajte 100 bacanja igraće kocke (kocka s brojevima 1 do 6). Pomoću histograma prikažite rezultat ovih bacanja.
-------------------------------------------------------------------------------------------------------------------------------------------------------

Bacanje igraće kocke 100 puta smo simulirali generiranjem polja veličine 100 gdje smo svako polje popunili random brojem od 1 do 6. To smo postigli kao u prošlom zadatku, samo što smo ovdje definirali donju granicu (low) kao 1 i gornju granjicu (high) kao 7. Rezultate smo ispisali pomoću histograma kao i u prošlom zadatku. 



**Zadatak 5 **

U   direktoriju rusu_lv_2019_20/LV2/resources   nalazi   se   datoteka mtcars.csv   koja   sadrži   različita mjerenja  provedena  na  32  automobila  (modeli  1973-74).  Prikažite  ovisnost  potrošnje  automobila  (mpg)  o konjskim snagama  (hp).    Na  istom  grafu  prikažite  i  informaciju  o  težini  pojedinog  vozila.  Ispišite  minimalne,  maksimalne  i srednje vrijednosti potrošnje automobila.
-------------------------------------------------------------------------------------------------------------------------------------------------------

Podatke iz csv datoteke smo učitali u polje auti pomoću naredbe pd.read_csv() koja se nalazi u pandas biblioteci. Pojedinim vrijednostima iz csv datoteke smo pristupali pomoću operatora . , npr. potrošnji automobila smo pristupili s auti.mpg. Ovisnost dvaju stvari se prikazuje pomoću metode scatter iz matplotlib.pyplot biblioteke. Ona za argumente prima dvije carijable čiju ovisnost želimo prikazati. Također, u argument smo dodali i kako želimo da se boja mijenja u odnosu na treće svojstvo (težinu). Minimalnu, maksimalnu i srednju vrijednost potrošbnje automobila smo ispisali pomoću metoda min(), max() i diljenjem sum()/len() za prosječnu vrijednost. 



**Zadatak 6 **

Na  temelju  primjera  2.8.  učitajte  sliku  'tiger.png'.  Manipulacijom  odgovarajuće  numpy  matrice  pokušajte  posvijetliti sliku (povećati brightness).
-------------------------------------------------------------------------------------------------------------------------------------------------------

Sliku smo učitali u varijablu kopiranjem koda iz primjera 2.8., odnosno linijom img = mpimg.imread() iz biblioteke matplotlib.image. Naredbom plt.imshow() smo ju prikazali na ekranu. Sliku smo uspjeli posvjetliti tako što smo varijablu img pomnožili s brojem većim od 1, konkrento s brojem 2 što znači da smo dvostruko posvijetlili sliku. Množenjem s npr. brojem 0.5, dvostruko bi potamnili sliku. 