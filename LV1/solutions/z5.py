# -*- coding: utf-8 -*-
"""
Created on Thu Nov 26 22:18:47 2020

@author: matij
"""

lista = []
prosjek = 0
    
imeDatoteke = input ('Unesite ime tekstualne datoteke: ')
fhand = open(imeDatoteke)
for line in fhand: 
    line = line.rstrip()   
    if line.startswith('X-DSPAM-Confidence:'):
        lista.append (float(line[20:26]))

prosjek = sum(lista) / len(lista)

print('Ime datoteke: ', imeDatoteke)
print('Average X-DSPAM-Confidence: ', prosjek)