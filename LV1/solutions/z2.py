# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 14:38:44 2020

@author: student
"""

while True: 
    try:
        ocjena = float(input ('Unesite jedan broj koji predstavlja neku ocjenu, a da je imzeđu 0.0 i 1.0: '))
        while (ocjena > 1.0 or ocjena < 0.0):
            print('Niste unijeli odgovarajući broj, unesite broj između 0.0 i 1.0.')
            break       
    except ValueError:
            print('Niste unijeli broj!')

if (ocjena >= 0.9):
    print('Ocjena pripada A kategoriji.')
elif (ocjena >= 0.8):
    print('Ocjena pripada B kategoriji.')
elif (ocjena >= 0.7):
    print('Ocjena pripada C kategoriji.')
elif (ocjena >= 0.6):
    print('Ocjena pripada D kategoriji.')
elif (ocjena < 0.6):
    print('Ocjena pripada F kategoriji.')    

