# -*- coding: utf-8 -*-
"""
Created on Mon Nov 16 15:15:34 2020

@author: student
"""

lista = []

while True:
    broj = input('Unesite broj: ')
    if (broj == "Done"):
        break
    try:
          lista.append(float(broj))
    except: 
        print('Niste upisali broj.')

brojac = len(lista)
minimum = min(lista)
maksimum = max(lista)
srednjaVrijednost = sum(lista)/brojac

print('Upisano brojeva: ', brojac)
print('Minimalna vrijednost: ', minimum)
print('Maksimalna vrijednost: ', maksimum)
print('Srednja vrijednost: ', srednjaVrijednost)
    
