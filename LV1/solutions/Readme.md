Ovaj repozitorij sadrži python skriptu koja broji koliko puta se pojedina riječ pojavljuje u tekstualnoj datatoteci. Rezultat bi trebao biti dictionary gdje uz svaku riječ (key) stoji broj ponavljanja riječi (value). Međutim, skripta ima bugove i ne radi kako je zamišljeno.

Promjene:

1. raw_input prepravljeno u input
2. fnamex prepravljeno u fname
3. print counts prepravljeno u print (counts)





Opis vježbe:
U ovoj vježbi smo naučili kako spremati i ispivati podatke, koristiti razne aritmetičke operatore, učitavati podatke iz dokumenta i zapisivati ih u listu, try i except naredbe, rad sa funkcijama, operacije sa stringovima i rad s dictionary elementima.


Zadatak 1
U ovom zadatku morali smo korisniku osigurati unos dva broja pomoću naredbe input, castali smo oba broja u tip podataka float kako bi izbjegli kasnije probleme. Cilj zadatka je bio ispis umnoška ta dva broja, odnosno zarade koja je umnožak broja sati i plaće po satu.

Zadatak 2
U ovom zadatku morali smo korisniku osigurati unos jednog broja između 0.0 i 1.0 što smo osigurali unutar try-except naredbe u jednoj while petlji. Ako nije unesen traženi broj, korisniku se nudi opcija ponovnog unosa broja. S obzirom na to u koji interval spada korisnički unesen broj, program ispisuje na ekran kategoriju A, B, C, D ili F. To smo postigli if i elif naredbama.

Zadatak 3
U ovom zadatku morali smo modificirati kod iz Zadatka 1 tako da radi istu stvar, ali da se ukupan iznos izračunava u zasebnoj funkciji total_kn. Definirali smo total_kn funkciju tako da prima dva argumenta (broj radnih sati i plaću po satu) te onda samo pomnoži ta dva broja i ispisuje taju množak na ekran.

Zadatak 4
U ovom zadatku morali smo korisniku osigurati unos proizvoljnog broja brojeva, sve dok korisnik ne upiše riječ "Done". To smo osigurali koristeći while True beskonačnu petlju u kojoj na početku prvo provjeravamo glasi li uneseni string "Done". Ako nije, onda appendamo taj broj u kreiranu listu brojeva i onda ide dalje s unosom brojeva, a ako je unesen "Done", onda se petlja prekida. Nakon što je petlja prekinuta, računamo broj unesenih brojeva preko funkcije len(), minimalnu i maksimalnu vrijednost pomoću funkcija min(), odnosno max() te srednju vrijednost svih elemenata liste (tj. unesenih brojeva) pomoću količnika zbroja liste( sum() ) i ukupnog broja elemenata liste. Sve to zatim ispišemo na ekranu.

Zadatak 5
U ovom zadatku morali smo korisniku osigurati unos naziva datoteke koju želi otovriti. To smo uspjeli pomoću naredbe input i spremanja inputa u jednu varijablu te zatim pomoću funkcije open otvaranja te datoteke. Zatim, morali smo naći linije koji u sebi sadrže neki određeni string i to smo uspjeli pomoću funkcije line.startswith(). Iz te linije smo morali isčitati i spremiti broj koji se nalazi poslije X-DSPAM-Confidence:, a to smo uspjeli naredbom lista.append(float(line[20:26])) jer do dvotočke imamo 19 znakova a nas zanima broj od 6 znamenaka koji se nalazi poslije. Svaki taj broj appendamo u listu. Nakon što program prođe kroz cijelu listu, moramo izračunati prosječnu vrijednost tog broja a to postižemo dijeljenjem zbroja elemenata liste i brojem elemenata liste. Zatim to ispišemo na ekran.

Zadatak 6
U ovom zadatku morali smo korisniku osigurati unos naziva datoteke koju želi otvoriti. To smo postigili isto kao i u prethodnom zadatku. Zatim, morali smopronaći redove koji počinju s "From:" i iz tih redova izdvojiti e-mail adrese a to smo uspjeli ovom naredbom: host.find('@') koja pronalazi stringove koji sadrže '@', te hostovi.append(line[num+1:]) kojim appendamo ostatak stringa koji se nalazi poslije znaka @. Nadalje, trebali smo izračunati koliko se puta koji hostname pojavljuje u dokumentu. Bilo je 6 hostnamea i za svaki smo definirali brojač i prvotno ga postavili na nulu. Zatim, prošli smo po listi i ako linija počinje s hostnameom inkrementirali smo taj odgovarajući brojač. Kad smo prošli kroz cijelu listu, izračunali smo koliko se puta koji hsotname pojavljuje. Na kraju, morali smo napraviti dictionary koji za key-value vrijednosti ima "ime hosta"-broj pojavljanja. Dictionary smo definirali s naredbom host = dict(). Primjer za uct.ac.za hostname je host['uct.ac.za'] = brojac_uct, gdje brojac_uct predstavlja broj ponavljanja uct.ac.za hostnamea. Također, ispisali smo nekoliko elemenata liste pomoću naredbe print(lista[0:3]), a dictionary smo ispisali pomoću naredbe print(host). 